# Java fundamentals

## Day 1

- Hello World
- created package , class interface
- created maven project
- constructors
- private, public
- inheritance
```java
package com.company.zoo;
```
- reading from keyboard using scanner

## Day 2

- String, String.format, StringBuilder
- overwriting overloading
- getter setters
- Book class
- reading from files using Scanner
- parseint 
- type parameters
```java
package com.company.books;

public class Box<T> {

   final private T boxContent;

   public Box(T content) {
		this.boxContent = content;
	}	
	public T getContent() {
		return boxContent;
	}
}
```

## Day 3

- collections
  - Lists, ArrayList
  - Map, HashMap
- collections manipulation
`com.company.books.App`
  - lambda `com.company.books.App:62`
- update Book class
- create fluent builder
- create constructor with parameters
- added maven dependency (mysql-java-connector)
- read persons from database

## Day 4

- added maven dependency (selenium)
- read web page using selenium
- create books using fluent builder from webpage data
- INSERT books into database
- UPSERT books in database

## Freedom

See you next time

## How to execute

- start database `docker run --name mysql -e MYSQL_ROOT_PASSWORD=root -p "3306:3306" -d mysql:8`
- start selenium `docker run -d --restart always --name selenium_vnc -p "4444:4444" -p "6901:6901" alexproca/selenium-vnc`
- create databases from sql directory
- run `gradle execute -PmainClass=com.company.books.GetBooks`