package com.company.hello;

import java.util.Scanner;

public class HelloWorld {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		
		while(true) {

			System.out.println("Introduceti numele acum!");
			String name = keyboard.nextLine();
			
			if("EXIT".equals(name.toUpperCase())) {
				System.out.println("Bye!");
				break;
			}

			System.out.println("Hello " + name + "!");

		}

	}

}
