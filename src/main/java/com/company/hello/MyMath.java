package com.company.hello;

import java.util.Scanner;

public class MyMath {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Valori posibile: adunare, scadere, exit");
		boolean loop = true;

		while (loop) {
			
			String instructiune = keyboard.nextLine();	
			
			switch (instructiune) {

			case "adunare":
				sumaAdouaNumere(keyboard);
				break;
				
			case "scadere":
				diferentaAdouaNumere(keyboard);
				break;
				
			case "exit":
				loop = false;
				break;

			default:
				System.out.println("Valori posibile: adunare, scadere, exit");
				break;
			}
		}

	}

	public static void sumaAdouaNumere(Scanner keyboard) {

		int a, b;

		while (true) {
			System.out.println("Două numere acum!");
			a = keyboard.nextInt();
			b = keyboard.nextInt();
			
			int suma = a +b;

			if (suma == 0) {
				System.out.println("Bye!");
				break;
			}

			if (suma > 0) {
				System.out.println("Suma este pozitiva: " + suma);
			} else {
				System.out.println("Suma este negativa: " + suma);
			}
		}
	}

	public static void diferentaAdouaNumere(Scanner keyboard) {

		int a, b;

		while (true) {
			System.out.println("Două numere acum!");
			a = keyboard.nextInt();
			b = keyboard.nextInt();

			int diferenta = a - b;

			if (diferenta == 0) {
				System.out.println("Bye!");
				break;
			}

			if (diferenta > 0) {
				System.out.println("Diferenta este pozitiva: " + diferenta);
			} else {
				System.out.println("Diferenta este negativa: " + diferenta);
			}
		}

	}

}
