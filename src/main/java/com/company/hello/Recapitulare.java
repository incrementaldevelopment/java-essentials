package com.company.hello;

import java.util.Scanner;

public class Recapitulare {

	public static void main(String[] args) {
		int suma = 3;
		
		suma++;
		
		System.out.println(suma);
		
		suma += 1;
		
		System.out.println(suma);
		
		boolean test = true ^ true;
		
		System.out.println(test);
		
		System.out.println("False: " + !true);
		
		System.out.println("True: " + (true && true) );
		
		System.out.println("False: " + (false && true) );

		System.out.println("True: " + (false || true) );
		
		System.out.println("True: " + (true || false) );
		
		boolean condition = false;
		
		System.out.println("Ternary: " + ( condition ? "Este adevarată": "Nu este adevarată" ));
		
		if(condition) {
			System.out.println("Non Ternary:  Este adevarată");
			
		} else {
			System.out.println("Non Ternary: Nu este adevarată");
			
		}
		
		Scanner keyboard = new Scanner(System.in);
		
		int value = keyboard.nextInt();
		
		switch (value) {
		case 1:
		case 3:
		case 5:
		case 7:
			System.out.println("Impar");
			break;
		case 2:
		case 4:
		case 6:
		case 8:	
			System.out.println("Par");

		default:
			break;
		}
		
		int k =0;
		
		do {
			
			k += 1;
			
			System.out.print("k=" + k);
			
			if(k % 2 != 0) {
				System.out.println("");
				continue;
			}
			
			System.out.println(" este Par");
			
		} while(k <= 10);
		
		
	}
	
}
