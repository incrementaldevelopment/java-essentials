package com.company.collections;

import java.util.Arrays;

public class MyStrings {

	public static void main(String[] args) {
		
		String test = "unu doi trei patru cinci";
		
		System.out.println(test.substring(4,	 7));
		
		String[] splitted = test.split("\\s");
		
		System.out.println(Arrays.toString(splitted));
		
		int n = 3;
		
		System.out.println(splitted[n - 1]);
		
		char[] charArray = test.toCharArray();
		for(Character c : charArray) {
			System.out.print(String.format("%1$c%1$c%4$.2f", c, 2, 3 ,4.56787, 5, 6, 7, 8));
		}
		
	}
	
}
