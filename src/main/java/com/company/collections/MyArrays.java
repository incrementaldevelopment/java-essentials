package com.company.collections;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class MyArrays {

	public static void main(String[] args) throws FileNotFoundException {

		String fileName = "./arrays/Arrays.txt";
		if(args.length > 0) {
			System.out.println(Arrays.toString(args));
			fileName = args[0];
		}
		
		
		Scanner keyboard = new Scanner(new File(fileName));

		int n = keyboard.nextInt();
		
		int[] myArray = new int[n];
		
		System.out.println( String.format("Reading %d numbers from '%s'", n, fileName) );
		
		for(int i=0;i<n;i++) {
			int currentValue = keyboard.nextInt();
			myArray[i] = currentValue;
		}
		
		toString(myArray);
	}
	
	public static void toString(int[] a) {
		System.out.print("[");
		for(int i=0;i<a.length;i++) {
			System.out.print(a[i]);
			
			boolean endOfArray = i + 1 == a.length;
			if(endOfArray) {
				System.out.print("]");
			} else {
				System.out.print(", ");				
			}
		}
	}
	
}
