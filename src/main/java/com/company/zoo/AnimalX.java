package com.company.zoo;

public class AnimalX implements TalkingAnimal {

	@Override
	public String talk() {
		return "X Boo";
	}

	@Override
	public void eat() {
		System.out.println("Animal X Eating");
	}

}
