package com.company.zoo;

public interface TalkingAnimal {

	String talk();
	void eat();
	
}
