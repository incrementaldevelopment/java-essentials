package com.company.zoo;

public class AnimalY implements TalkingAnimal {

	final private String type;
	private int age;
	
	public AnimalY(String type) {
		this.type = type;
		this.age = 0;
	}
	
	public AnimalY(String type, int age) {
		this.type = type;
		this.age = age;
	}
	
	public String talk() {
		return "Y Boo";
	}
	
	public void eat() {
		System.out.println("Eating");
	}
}
