package com.company.zoo;

public abstract class AnimalCarnivor implements TalkingAnimal {

	@Override
	public void eat() {
		System.out.println("Eating meat");
	}
	
}
