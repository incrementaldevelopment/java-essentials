package com.company.books;

import java.io.ByteArrayInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class GetBooks {

	public static void main(String[] args) throws Exception {

		String booksUrl = "https://www.goodreads.com/list/show/13086.Goodreads_Top_100_Literary_Novels_of_All_Time";

		List<Book> downloadedBooks = downloadBooks(booksUrl);
		
		for(Book b : downloadedBooks) {
			System.out.println(b);
		}
	}

	public static List<Book> downloadBooks(String booksUrl) throws MalformedURLException, Exception {
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();

		RemoteWebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);

		driver.get(booksUrl);

		// #all_votes > table > tbody > tr

		List<WebElement> bookElements = driver.findElements(By.cssSelector("#all_votes > table > tbody > tr"));

		List<Book> downloadedBooks = new ArrayList<>();
		
		try {
			for (WebElement currentRow : bookElements) {

				String bookText = currentRow.getText();

				Scanner scanner = new Scanner(new ByteArrayInputStream(bookText.getBytes()));

				Book book = new BookBuilder()
						.addBookNumber(scanner.nextLine())
						.addTitle(scanner.nextLine())
						.addAuthor(scanner.nextLine().substring(3))
						.parseRatingString(scanner.nextLine())
						.build();

				downloadedBooks.add(book);
				System.out.println(book);
//				System.out.println("======================");
				scanner.close();

			}
		} catch (Exception e) {
			throw e;
		} finally {
			driver.close();
		}
		return downloadedBooks;
	}
}
