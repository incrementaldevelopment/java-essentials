package com.company.books;

import javax.naming.OperationNotSupportedException;

public class BookBuilder {

	private int bookNumber;
	private String title;
	private String author;
	private String rating;
	private String numberOfRaters;

	public BookBuilder addBookNumber(int bookNumber) {
		this.bookNumber = bookNumber;
		return this;
	}

	public BookBuilder addTitle(String title) {
		this.title = title;
		return this;
	}

	public BookBuilder addAuthor(String author) {
		this.author = author;
		return this;
	}

	public BookBuilder addRating(String rating) {
		this.rating = rating;
		return this;
	}

	public BookBuilder addNumberOfRaters(String numberOfRaters) {
		this.numberOfRaters = numberOfRaters;
		return this;
	}

	public BookBuilder addNumberOfRaters(int numberOfRaters) {
		this.numberOfRaters = "" + numberOfRaters;
		return this;
	}

	public Book build() {

		if (bookNumber == 0) {
			throw new RuntimeException("Book number must be set");
		}

		if (title == null) {
			throw new RuntimeException("Book title must be set");
		}

		if (author == null) {
			throw new RuntimeException("Book author must be set");
		}

		if (rating == null) {
			throw new RuntimeException("Book rating must be set");
		}

		if (numberOfRaters == null) {
			throw new RuntimeException("Book numberOfRaters must be set");
		}

		return new Book(bookNumber, title, author, rating, numberOfRaters);
	}

	public BookBuilder addBookNumber(String bookNumberAsString) {
		this.bookNumber = Integer.parseInt(bookNumberAsString);
		return this;
	}

	public BookBuilder parseRatingString(String nextLine) {
		String[] ratingData = nextLine.split(" avg rating — ");
		
		addRating(ratingData[0]);		
		addNumberOfRaters(trimLastCharacters(ratingData[1], 8));
		
		return this;
	}
	
	private static String trimLastCharacters(String stringToBeTrimmed, int numberOfTrimmedCharacters) {
		return stringToBeTrimmed.substring(0, stringToBeTrimmed.length() - numberOfTrimmedCharacters);
	}

}
