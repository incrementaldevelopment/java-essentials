package com.company.books;

public class Box<T> {

	final private T boxContent;
	
	public Box(T content) {
		this.boxContent = content;
	}
	
	public T getContent() {
		return boxContent;
	}
	
}
