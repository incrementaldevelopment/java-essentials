package com.company.books;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class App {

	public static void main(String[] args) throws FileNotFoundException {

		String fileName = "./books/First2Books.txt";
		fileName = "./books/GoodReads.txt";

		if (args.length > 0) {
			fileName = args[0];
		}

		List<Book> best100 = readBookFromFile(fileName);

		Map<String, Book> booksByTitle = new HashMap<>();

		int maxLengthOfBookTitle = computeMaxLengthOfBookTitle(best100);

		for (Book cb : best100) {
			booksByTitle.put(cb.getTitle().toLowerCase(), cb);
		}

		String bookTitle = "How to learn things";
			bookTitle = "The great gatsby";

		String stringTemplate = "Autor: %" + maxLengthOfBookTitle + "s -> Carte: %s";
		Book cb = booksByTitle.get(bookTitle.toLowerCase());

		if (cb != null) {
			System.out.println(String.format(stringTemplate, cb.getAuthor(), cb.getTitle()));
		} else {
			System.out.println(String.format("There is no such a book named: '%s'", bookTitle));
		}

		Map<String, Integer> numberOfBooksByAuthor = getNumberOfBooksByAuthor(best100);
		
		// Varianta normala
		System.out.println("Varianta fara lambda");
		for(String author: numberOfBooksByAuthor.keySet()) {
			Integer numberOfBooks = numberOfBooksByAuthor.get(author);
			if(numberOfBooks > 1) {
				System.out.println(String.format("Author: %35s -> %d", author, numberOfBooks));				
			}
		}
		
		//varianta cu lambda
		
		System.out.println("Varianta cu lambda");
		
		Predicate<? super Entry<String, Integer>> moreThanOneBookPredicate = entry -> entry.getValue() > 1;
		Consumer<? super Entry<String, Integer>> printBooks = entry -> System.out.println(String.format("Author: %35s -> %d", entry.getKey(), entry.getValue()));

		numberOfBooksByAuthor.entrySet()
			.stream()
			.filter(moreThanOneBookPredicate)
			.forEach(printBooks);
	}

	private static Map<String, Integer> getNumberOfBooksByAuthor(List<Book> best100) {
		Map<String, Integer> numberOfBooksByAuthor = new HashMap<>();
		
		for (Book bba : best100) {
			Integer numberOfBooks = numberOfBooksByAuthor.get(bba.getAuthor());
			
			if(numberOfBooks != null) {
				numberOfBooksByAuthor.put(bba.getAuthor(), numberOfBooks + 1);
			} else {
				numberOfBooksByAuthor.put(bba.getAuthor(), 1);	
			}
		}
		return numberOfBooksByAuthor;
	}

	private static int computeMaxLengthOfBookTitle(List<Book> best100) {
		int maxLengthOfBookTitle = 0;

		for (Book cb : best100) {

			int titleLength = cb.getAuthor().length();

			if (titleLength > maxLengthOfBookTitle) {
				maxLengthOfBookTitle = titleLength;
			}
		}
		return maxLengthOfBookTitle;
	}

	private static List<Book> readBookFromFile(String fileName) throws FileNotFoundException {
		Scanner scanner = new Scanner(new File(fileName));

		List<Book> best100 = new ArrayList<>();

		while (scanner.hasNextLine()) {
			Book currentBook = Book.readNextBook(scanner);
			best100.add(currentBook);
		}
		return best100;
	}

}
