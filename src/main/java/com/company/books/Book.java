package com.company.books;

import java.util.Scanner;

public class Book {
	
	private final int bookNumber;
	private final String title;
	private final String author;
	private final String rating;
	private final String numberOfRaters;
	
	static private String classId = "3";
	
	public Double getRatingAsDouble() {
		return Double.parseDouble(rating);
	}
	
	public Integer getNumberOfRatersAsInteger() {
		return Integer.parseInt(numberOfRaters.replace(",", "").trim());
	}
	
	public int getBookNumber() {
		return bookNumber;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getRating() {
		return rating;
	}

	public String getNumberOfRaters() {
		return numberOfRaters;
	}

	public Book() {
		this.bookNumber = 1;
		this.title = "Java for Experts";
		this.author = "Joe Black";
		this.rating = "256";
		this.numberOfRaters = "1024";
	}
	
	public Book(int bookNumber, String title, String author, String rating, String numberOfRaters) {
		this.bookNumber = bookNumber;
		this.title = title;
		this.author = author;
		this.rating = rating;
		this.numberOfRaters = numberOfRaters;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder()
				.append(String.format("Book #: %d\n", bookNumber))
				.append(String.format("Title: %s\n", title))
				.append(String.format("Author: %s\n", author))
				.append(String.format("Rating: %s\n", rating))
				.append(String.format("Raters #: %d\n", getNumberOfRatersAsInteger()))
				;
		
		return builder.toString();
	}

	static public Book readNextBook(Scanner scanner) {

		BookBuilder builder = new BookBuilder();
		builder.addBookNumber(scanner.nextLine().trim());
		
		skipLines(scanner, 1);
		
		builder.addTitle(scanner.nextLine().trim());
		builder.addAuthor(scanner.nextLine().substring(3));
		builder.parseRatingString(scanner.nextLine());
		
		skipLines(scanner, 7);
		
		return builder.build();
	}
	
	private static void skipLines(Scanner scanner, int n) {
		for(int i =0;i<n;i++) {
			scanner.nextLine();
		}
	}
	
	
}
