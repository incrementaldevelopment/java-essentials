package com.company.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import com.company.books.Book;
import com.company.books.GetBooks;

public class WriteBooks {

	public static void main(String[] args) throws Exception {
		
		Connection connection = null;
		PreparedStatement readStatement = null;
		PreparedStatement updateStatement = null;
		ResultSet resultSet = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			connection = DriverManager.getConnection("jdbc:mysql://localhost/library?user=root&password=root");

			String sql = "INSERT INTO `library`.`books` (`bookNumber`, `title`, `author`, `rating`, `numberOfRaters`) VALUES (?, ?, ?, ?, ?);";
			String upsertSql = "INSERT INTO `library`.`books` (`bookNumber`, `title`, `author`, `rating`, `numberOfRaters`, `id`) "
					+ "VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `rating` = ?, `numberOfRaters` = ?;";
			
			updateStatement = connection.prepareStatement(upsertSql);
			
			String booksUrl = "https://www.goodreads.com/list/show/13086.Goodreads_Top_100_Literary_Novels_of_All_Time";

			List<Book> downloadedBooks = GetBooks.downloadBooks(booksUrl);
			
			for(Book b : downloadedBooks) {
				
				updateStatement.setInt(1, b.getBookNumber());
				updateStatement.setString(2, b.getTitle());
				updateStatement.setString(3, b.getAuthor());
				updateStatement.setDouble(4, b.getRatingAsDouble());
				updateStatement.setInt(5, b.getNumberOfRatersAsInteger());
				updateStatement.setInt(6, b.getBookNumber());

				updateStatement.setDouble(7, b.getRatingAsDouble());
				updateStatement.setInt(8, b.getNumberOfRatersAsInteger());
				
				updateStatement.executeUpdate();
				
//				System.out.println(b);
			}
			
			
		} catch (Exception e) {
			throw e;
		} finally {
			if(connection != null) {
				connection.close();
			}
			if(readStatement != null) {
				readStatement.close();
			}
			if(resultSet != null) {
				resultSet.close();
			}
			if(updateStatement != null) {
				updateStatement.close();
			}
		}
	}
}
