package com.company.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ReadPersons {

	public static void main(String[] args) throws Exception {
		
		Connection connection = null;
		PreparedStatement readStatement = null;
		ResultSet resultSet = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			connection = DriverManager.getConnection("jdbc:mysql://localhost/library?user=root&password=root");
			
			readStatement = connection.prepareStatement("SELECT name, surname, age FROM library.persons;");
			
			resultSet = readStatement.executeQuery();
			
			while(resultSet.next()) {
				
				StringBuilder builder = new StringBuilder()
						.append(String.format("Name: %s\n", resultSet.getString("name")))
						.append(String.format("Surname: %s\n", resultSet.getString("surname")))
						.append(String.format("Age: %d\n", resultSet.getInt("age")))
						.append("=================================================")
						;
				System.out.println(builder.toString());
			}
			
			
		} catch (Exception e) {
			throw e;
		} finally {
			if(connection != null) {
				connection.close();
			}
			if(readStatement != null) {
				readStatement.close();
			}
			if(resultSet != null) {
				resultSet.close();
			}
		}
	}
}
