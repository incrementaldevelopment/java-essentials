CREATE TABLE `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bookNumber` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `numberOfRaters` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4;
